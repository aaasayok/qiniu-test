<?php
/**
 * Created by PhpStorm.
 * User: wang
 * Date: 15-7-12
 * Time: 下午5:59
 */

namespace Upload;

use Qiniu\Auth;

class QiNiu {

    private $accessKey = '80OtErHlY4BGyA74yABbVXtzP6cgv7yQnNaI_Frg';

    private $secretKey = 'WLufcvEbjqdx_GmZewrLqyhdsLWbx1Ss06xdk5wP';

    private $bucket    = 'images';   //上传的空间名

    private $qiNiuHost = 'http://upload.qiniu.com/';


    public function __construct()
    {

    }

    /**
     * 七牛上传
     */
    public function upload($file)
    {
        $auth      = new Auth($this->accessKey, $this->secretKey);

        $token     = $auth->uploadToken($this->bucket);

        if (class_exists('\CURLFile')) {
            $data = array('file' => new \CURLFile(realpath($file['tmp_name'])));
        } else {
            $data = array('file' => '@' . realpath($file['tmp_name']));
        }

       $data['token'] = $token;

        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, $this->qiNiuHost);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_POST,true);
        if (class_exists('\CURLFile')) {
            curl_setopt($ch, CURLOPT_SAFE_UPLOAD, true);
        } else {
            if (defined('CURLOPT_SAFE_UPLOAD')) {
                curl_setopt($ch, CURLOPT_SAFE_UPLOAD, true);
            }
        }
        curl_setopt($ch,CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result);
    }

}